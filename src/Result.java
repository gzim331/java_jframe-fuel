import javax.swing.*;
import java.awt.*;
import java.awt.Graphics;

public class Result extends JFrame  {

    private JLabel lPrice, lFrom, lWhere;

    public Result() {
        setSize(380,325);
        setTitle("Result");
        setLayout(null);
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        lWhere = new JLabel();
        lWhere.setBounds(60,10, 300, 40);
        lWhere.setFont(new Font("SansSerif", Font.PLAIN, 22));
        add(lWhere);

        lFrom = new JLabel();
        lFrom.setBounds(60,245, 300, 40);
        lFrom.setFont(new Font("SansSerif", Font.PLAIN, 22));
        add(lFrom);

        lPrice = new JLabel();
        lPrice.setBounds(120,120, 300, 40);
        lPrice.setFont(new Font("SansSerif", Font.BOLD, 23));
        lPrice.setForeground(Color.BLUE);
        add(lPrice);
    }

    public void showResult(String where, String from, String price) {
        lWhere.setText(where);
        lFrom.setText(from);
        lPrice.setText(price.substring(7));
    }

    public static void main(String[] args) {
        Result app = new Result();
    }

    public void paint(Graphics g) {
        super.paint(g);
        getGraphics().fillOval(60,245,20,20);
        g.drawLine(70, 95, 70, 245);
        getGraphics().fillOval(60,75,20,20);
    }
}