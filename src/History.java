import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class History extends JFrame {

    private JTextArea field;

    public History() {
        setSize(400,600);
        setTitle("History");
        setLayout(null);
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        field = new JTextArea();
        JScrollPane scrollPane = new JScrollPane(field);
        scrollPane.setBounds(10,10,380,555);
        add(scrollPane);
    }

    public void showHistory() {
        String fileName = "history.txt";
        File file = new File(fileName);

        try {
            Scanner scanner = new Scanner(file);
            while(scanner.hasNext()) {
                field.append(scanner.nextLine() + "\n");
            }
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
    }

    public static void main(String[] args) { }
}
