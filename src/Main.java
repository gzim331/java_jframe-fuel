import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.Date;
import java.util.Scanner;

public class Main extends JFrame implements ChangeListener, ActionListener {

    private JMenuBar menuBar;
    private JMenu menuFuel, menuHelp;
    private JMenuItem mSave, mHistory, mExit, mAbout;

    private JLabel lTrasa, lSpalanie, lCena, lDodatkowyCiezar, lTytul, lWynik, lAuto, lFrom, lWhere;
    private JTextField tFrom, tWhere;
    private JSlider sTrasa, sSpalanie, sCena, sDodatkowyCiezar;
    private JComboBox cbAuto;
    private double trasa, spalanie, cena, ciezar;

    private Image image;
    private String filename = "image/index.png";

    public Main() {
        setSize(1000,600);
        setTitle("Fuel");
        setLayout(null);

        menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        menuFuel = new JMenu("Fuel");
        menuBar.add(menuFuel);

        mSave = new JMenuItem("Save");
        mSave.addActionListener(this);
        mSave.setAccelerator(KeyStroke.getKeyStroke("ctrl S"));
        menuFuel.add(mSave);

        mHistory = new JMenuItem("History");
        mHistory.setAccelerator(KeyStroke.getKeyStroke("ctrl H"));
        mHistory.addActionListener(this);
        menuFuel.add(mHistory); //gdzies tutaj pokombinowac

        menuFuel.addSeparator();
        mExit = new JMenuItem("Wyjscie");
        mExit.setAccelerator(KeyStroke.getKeyStroke("ctrl X"));
        mExit.addActionListener(this);
        menuFuel.add(mExit);

        menuHelp = new JMenu("Help");
        menuBar.add(menuHelp);

        mAbout = new JMenuItem("About");
        mAbout.addActionListener(this);
        menuHelp.add(mAbout);


        lTytul = new JLabel("Przelicznik drogi na cenę");
        lTytul.setBounds(230,10,600, 30);
        lTytul.setFont(new Font("SansSerif", Font.BOLD, 25));
        add(lTytul);

        lTrasa = new JLabel("Trasa:");
        lTrasa.setBounds(100, 80, 150, 20);
        add(lTrasa);

        sTrasa = new JSlider(0,700, 0);
        sTrasa.setBounds(100, 100, 400, 20);
        sTrasa.setMinorTickSpacing(100);
        sTrasa.setPaintTicks(true);
        sTrasa.setPaintLabels(true);
        sTrasa.addChangeListener(this);
        add(sTrasa);

        lSpalanie = new JLabel("Spalanie:");
        lSpalanie.setBounds(100, 140, 150, 20);
        add(lSpalanie);

        sSpalanie = new JSlider(0, 30,0);
        sSpalanie.setBounds(100, 160, 400, 20);
        sSpalanie.setMinorTickSpacing(5);
        sSpalanie.setPaintTicks(true);
        sSpalanie.setPaintLabels(true);
        sSpalanie.addChangeListener(this);
        add(sSpalanie);

        lCena = new JLabel("Cena:");
        lCena.setBounds(100, 200, 150, 20);
        add(lCena);

        sCena = new JSlider(2,22,2);
        sCena.setBounds(100, 220, 400, 20);
        sCena.setMinorTickSpacing(4);
        sCena.setPaintTicks(true);
        sCena.setPaintLabels(true);
        sCena.addChangeListener(this);
        add(sCena);

        lDodatkowyCiezar = new JLabel("Dodatkowy ciężar:");
        lDodatkowyCiezar.setBounds(100, 260, 200, 20);
        add(lDodatkowyCiezar);

        sDodatkowyCiezar = new JSlider(0, 600, 0);
        sDodatkowyCiezar.setBounds(100, 280, 400, 20);
        sDodatkowyCiezar.setMinorTickSpacing(50);
        sDodatkowyCiezar.setPaintTicks(true);
        sDodatkowyCiezar.setPaintLabels(true);
        sDodatkowyCiezar.addChangeListener(this);
        add(sDodatkowyCiezar);

        lAuto = new JLabel("Domyślne spalanie");
        lAuto.setBounds(100,320,150,20);
        add(lAuto);

        cbAuto = new JComboBox();
        cbAuto.setBounds(100, 340, 180, 20);
        cbAuto.addItem("Wybierz Samochód");
        cbAuto.addItem("Mustang GT");
        cbAuto.addItem("Mustang EcoBoost");
        cbAuto.addItem("Porsche 964");
        cbAuto.addItem("Cadillac DeVille '61");
        cbAuto.addItem("Aston Martin Vantage");
        cbAuto.addItem("Buick Riviera '63");
        cbAuto.addItem("Multipla");
        add(cbAuto);
        cbAuto.addActionListener(this);

        lFrom = new JLabel("Skąd");
        lFrom.setBounds(100, 380, 150, 20);
        add(lFrom);

        tFrom = new JTextField();
        tFrom.setBounds(100, 400, 200, 20);
        tFrom.setText(" ");
        add(tFrom);

        lWhere = new JLabel("Dokąd");
        lWhere.setBounds(340, 380, 150, 20);
        add(lWhere);

        tWhere = new JTextField();
        tWhere.setBounds(340, 400, 200,20);
        tWhere.setText(" ");
        tWhere.addActionListener(this);
        add(tWhere);

        lWynik = new JLabel("Cena");
        lWynik.setBounds(100, 450, 220, 40);
        lWynik.setFont(new Font("SansSerif", Font.PLAIN, 20));
        lWynik.setForeground(Color.BLUE);
        add(lWynik);
    }

    public static void main(String[] args) {
        Main app = new Main();
        app.setDefaultCloseOperation(EXIT_ON_CLOSE);
        app.setVisible(true);
    }

    public double przelicz(double trasa, double spalanie, double cena, double ciezar) {
        double ciezar_w = (0.6*ciezar)/100;
        double wynik = (((spalanie+ciezar_w)*trasa)/100)*cena;

        return wynik;
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        trasa = sTrasa.getValue();
        lTrasa.setText("Trasa: " + trasa + " km");
        spalanie = sSpalanie.getValue();
        lSpalanie.setText("Spalanie: " + spalanie + " L");
        cena = sCena.getValue();
        lCena.setText("Cena: " + cena + " PLN");
        ciezar = sDodatkowyCiezar.getValue();
        lDodatkowyCiezar.setText("Dodatkowy ciężar: " + ciezar + " kg");

        lWynik.setText("Cena = " + String.format("%.2f", przelicz(trasa, spalanie, cena, ciezar)) + " PLN");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();

        if (source == mSave) {

            int odpSave = JOptionPane.showConfirmDialog(null, "Czy na pewno chcesz zapisać?", "Save", JOptionPane.OK_OPTION);

            if (odpSave == JOptionPane.OK_OPTION) {

                String fileName = "history.txt";
                try{
                    Writer output = new BufferedWriter(new FileWriter(fileName, true));

                    Scanner scTrasa = new Scanner(lTrasa.getText());
                    Scanner scSpalanie = new Scanner(lSpalanie.getText());
                    Scanner scCena = new Scanner(lCena.getText());
                    Scanner scCiezar = new Scanner(lDodatkowyCiezar.getText());
                    Scanner scWynik = new Scanner(lWynik.getText());

                    while(scWynik.hasNext()) {
                        output.append(new Date().toString() + "\n");
                        output.append(scTrasa.nextLine() + "\n");
                        output.append(scSpalanie.nextLine() + "\n");
                        output.append(scCena.nextLine() + "\n");
                        output.append(scCiezar.nextLine() + "\n");
                        output.append(scWynik.nextLine() + "\n\n");
                    }

                    output.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        } else if (source == mHistory) {

            History history = new History();
            history.showHistory();

        } else if (source == mExit) {
            int odpExit = JOptionPane.showConfirmDialog(null, "Czy na pewno chcesz wyjść z programu?", "Exit", JOptionPane.YES_NO_OPTION);

            if (odpExit == JOptionPane.YES_OPTION) {
                dispose();
            } else if (odpExit == JOptionPane.NO_OPTION) {
                JOptionPane.showMessageDialog(null, "Zmieniłem zdanie, jednak idź stąd", "Exit", JOptionPane.INFORMATION_MESSAGE);
                dispose();
            }
        } else if (source == mAbout) {
            JOptionPane.showMessageDialog(null, "Program do obliczania ceny paliwa.\nby Michał Zimka", "About", JOptionPane.INFORMATION_MESSAGE);
        } else if(source == cbAuto) {

            String auto = cbAuto.getSelectedItem().toString();

            if(auto.equals("Mustang GT")) {
                sSpalanie.setValue(15);
                filename = "image/mustangGT.jpg";
                repaint();
            } else if (auto.equals("Mustang EcoBoost")) {
                sSpalanie.setValue(9);
                filename = "image/mustangEco.jpg";
                repaint();
            } else if (auto.equals("Porsche 964")) {
                sSpalanie.setValue(18);
                filename = "image/porsche964.jpg";
                repaint();
            } else if (auto.equals("Cadillac DeVille '61")) {
                sSpalanie.setValue(23);
                filename = "image/cadillac.jpg";
                repaint();
            } else if (auto.equals("Aston Martin Vantage")) {
                sSpalanie.setValue(16);
                filename = "image/aston.jpg";
                repaint();
            } else if (auto.equals("Buick Riviera '63")) {
                sSpalanie.setValue(18);
                filename = "image/buick.jpg";
                repaint();
            } else if (auto.equals("Multipla")) {
                sSpalanie.setValue(6);
                filename = "image/multipla.jpg";
                repaint();
            }
        } else if (source == tWhere) {
            String where = tWhere.getText();
            String from = tFrom.getText();
            String price = lWynik.getText();

            Result result = new Result();
            result.showResult(where, from, price);
        }
    }

    public void paint(Graphics g) {
        super.paint(g);
        ImageIcon img = new ImageIcon(filename);
        image = img.getImage();
        g.drawImage(image, 600, 140, 360, 230, null);
    }
}